// The module 'vscode' contains the VS Code extensibility API
// Import the necessary extensibility types to use in your code below
import {window, workspace, Disposable, ExtensionContext} from 'vscode';

let settings = getSettings();
// This method is called when your extension is activated. Activation is
// controlled by the activation events defined in package.json.
export function activate(context: ExtensionContext) {
    // Use the console to output diagnostic information (console.log) and errors (console.error).
    // This line of code will only be executed once when your extension is activated.
    console.log('Now logging active editor changes');

    settings = getSettings();
    let activeEditorLogger = new ActiveEditorLogger();
    let controller = new ActiveEditorController(activeEditorLogger);

    // Add to a list of disposables which are disposed when this extension is deactivated.
    context.subscriptions.push(controller);
    context.subscriptions.push(activeEditorLogger);
}
interface Settings {
    server: string;
    student_id: number;
    event_id: number;
    key: string;
}
function getSettings(){
    let config = workspace.getConfiguration('windowlog');
    const _server = config.get('server');
    const _event_id =config.get('event_id');
    const _student_id = config.get('student_id');
    const _key = config.get('key');
    let settings = {server: _server, event_id: _event_id, student_id: _student_id, key: _key} as Settings;
    return settings;
}
class ActiveEditorLogger {

    public logActiveEditor(settings: Settings) {

        // Get the current text editor
        let editor = window.activeTextEditor;
        if (!editor) {
            return;
        }

        // get the active filename and the date
        let fileName = editor.document.fileName;
        let unixTime = Math.round((new Date()).getTime() / 1000);
        var request = require('request');
        let url = 'http://' + settings.server + '/' + settings.event_id + '/' + settings.student_id + '/addwindowlog/';
        request.post(url).form({filename: fileName, timestamp: unixTime, key: settings.key});
        console.log(url + ' :: ' + fileName + ' :: ' + unixTime);
    }

    dispose() {
    }
}

class ActiveEditorController {

    private _activeEditorLogger: ActiveEditorLogger;
    private _disposable: Disposable;

    constructor(activeEditorLogger: ActiveEditorLogger) {
        this._activeEditorLogger = activeEditorLogger;

        // subscribe to selection change and editor activation events
        let subscriptions: Disposable[] = [];
        window.onDidChangeActiveTextEditor(this._onEvent, this, subscriptions);

        // log the active editor changes
        this._activeEditorLogger.logActiveEditor(settings);

        // create a combined disposable from both event subscriptions
        this._disposable = Disposable.from(...subscriptions);
    }

    dispose() {
        this._disposable.dispose();
    }

    private _onEvent() {
        this._activeEditorLogger.logActiveEditor(settings);
    }
}
